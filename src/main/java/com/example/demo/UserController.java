package com.example.demo;
import com.example.demo.User;
import com.example.demo.UserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users/")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping
    public List<User> getUser() {
        return userService.listAllUser();
    }

    @GetMapping("{id}")
    public Optional<User> getUserById(@PathVariable("id") Long id) {

        return userService.listUserById(id);
    }

    @PostMapping
    public String createUser(@RequestBody User user) {
        userService.createUser(user);
        return "user Created Successfully";
    }

    @DeleteMapping("{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
        return "User deleted : id - "+id ;
    }

}
