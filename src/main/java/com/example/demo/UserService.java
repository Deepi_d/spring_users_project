package com.example.demo;
import com.example.demo.User;
import com.example.demo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public void createUser(User user) {
        userRepository.save(user);
    }

    public  List<User> listAllUser() {
        return userRepository.findAll();
    }

    public  Optional<User> listUserById(Long id) {
        return userRepository.findById(id);
    }


    public void delete(Long id) {
        userRepository.deleteById(id);
    }
}
